from elasticsearch import Elasticsearch
from kafka import KafkaConsumer, KafkaClient
import avro.schema
import avro.io
import io

#setup consumer
consumer = KafkaConsumer('LOG', group_id = 'my_group', bootstrap_servers=['192.168.1.3:9092'])

#schema
schema = """
    {
        "namespace": "com.lnc.finance.event.log",
        "type": "record",
        "name": "KafkaLog",
        "fields": [
            {"name": "logger", "type": ["null", "string"], "default": null},
            {"name": "logLevel", "type": {"type": "enum", "name": "Level", "symbols": ["FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE"]}},
            {"name": "timestamp", "type": "long", "default": 0},
            {"name": "threadName", "type":  ["null", "string"], "default": null},
            {"name": "message", "type":  ["null", "string"], "default": null},
            {"name": "NDC", "type":  ["null", "string"], "default": null},
            {"name": "MDC", "type": ["null", {"type": "map", "values": "string"}], "default": null}
            ]
    }
    """
schema = avro.schema.parse(schema)

#elasticserver
es = Elasticsearch('192.168.1.3:9200')
#loop

for msg in consumer:
	bytes_reader = io.BytesIO(msg.value)
	# bytes_reader = io.BytesIO(bytearray(msg.value)[5:])
	decoder = avro.io.BinaryDecoder(bytes_reader)
	reader = avro.io.DatumReader(schema)
	txt = reader.read(decoder)
	es.index(index='record', doc_type='KakfaLog', body={'logger': txt['logger'],'logLevel': txt['logLevel'],'timestamp': txt['timestamp'],'threadName': txt['threadName'],'message': txt['message'],'NDC': txt['NDC'],'MDC': txt['MDC']})

	# print reader
	# print msg
	print txt
