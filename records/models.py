from django.db import models



schema = """
    {
        "namespace": "com.lnc.finance.event.log",
        "type": "record",
        "name": "KafkaLog",
        "fields": [
            {"name": "logger", "type": ["null", "string"], "default": null},
            {"name": "logLevel", "type": {"type": "enum", "name": "Level", "symbols": ["OFF", "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE", "ALL"]}},
            {"name": "timestamp", "type": "long", "default": 0},
            {"name": "threadName", "type":  ["null", "string"], "default": null},
            {"name": "message", "type":  ["null", "string"], "default": null},
            {"name": "NDC", "type":  ["null", "string"], "default": null},
            {"name": "MDC", "type": ["null", {"type": "map", "values": "string"}], "default": null}
            ]
    }
    """
# Create your models here.
class EventLog(models.Model):
	class Meta:
		managed = False




