from django.http import JsonResponse
from django.shortcuts import render
from elasticsearch import Elasticsearch
import datetime

# Create your views here.
ELASTICSEARCH_URL = 'localhost:9200' #localhost:9200


def getTime(string):
	try:
		long(string)	
	except:
		return 0
	else:
		return long(string)

	return 0

# def getPos(string):
# 	try:
# 		int(string)
# 		return int(string)
# 	except:
# 		print "invalid type"
# 	return 0


# def getSize(string):
# 	try:
# 			int(string)
# 			return int(string)
# 	except:
# 			print "invalid type"
# 	return 10

def home(request):

	return render(request, 'home.html', {})


def search(request):

	query = request.GET['q']
	pos= request.GET['pos']
	size = request.GET['size']
	level_q = request.GET['level']
	timefrom = getTime(request.GET['timeFrom'])
	timeto = getTime(request.GET['timeTo'])

	es = Elasticsearch(ELASTICSEARCH_URL)
	
	print pos
	print size 
	print "logLevel: " + level_q
	# print "from: %(timefrom)d to: %(timeto)d" % {"timefrom": timefrom, "timeto": timeto} 
	print query
	print timefrom
	print timeto

	
	if level_q != "TRACE":
		if not query: # query = ''
			if timefrom != 0 and timeto != 0:
				event = es.search(index="record", body={"query":
				{
					"bool": {
						"must": [
							{"match": {"logLevel": level_q}},
							{"range": {"timestamp": {"from": timefrom, "to": timeto}}}
						]
					}
					
				},
				"from":pos,
				"size":size
				})

			elif timefrom != 0 and timeto == 0:
				event = es.search(index="record", body={"query":
				{
					"bool": {
						"must": [
							{"match": {"logLevel": level_q}},
							{"range": {"timestamp": {"from": timefrom}}}
						]
					}
					
				},
				"from":pos,
				"size":size
				})
			elif timefrom == 0 and timeto != 0:
				event = es.search(index="record", body={"query":
				{
					"bool": {
						"must": [
							{"match": {"logLevel": level_q}}
						],
						"must_not": [
							{"range": {"timestamp": {"from": timeto}}}
						]
					}
					
				},
				"from":pos,
				"size":size
				})
			elif timefrom == 0 and timeto == 0:
				event = es.search(index="record", body={"query":
				{
					"bool": {
						"must": [
							{"match": {"logLevel": level_q}},
						]
					}
				},
				"from":pos,
				"size":size
				})

		else: #query != '', level_q != ALL
			if timefrom != 0 and timeto != 0:
				event = es.search(index="record", body={"query":
				{
					"bool": {
						"must": [
							{"match": {"logLevel": level_q}},
							{"match": {"message": query}}
						]
					},
					"range": {
						"timestamp": {
							"from": timefrom,
							"to": timeto
						}
					}
				},
				"from":pos,
				"size":size
				})

			elif timefrom != 0 and timeto == 0:
				event = es.search(index="record", body={"query":
				{
					"bool": {
						"must": [
							{"match": {"logLevel": level_q}},
							{"match": {"message": query}},
							{"range": {"timestamp": {"from": timefrom}}}
						]
					}
					
				},
				"from":pos,
				"size":size
				})

			elif timefrom == 0 and timeto != 0:
				event = es.search(index="record", body={"query":
				{
					"bool": {
						"must": [
							{"match": {"logLevel": level_q}},
							{"match": {"message": query}}
						],
						"must_not": [
							{"range": {"timestamp": {"from": timeto}}}
						]
					}
					
				},
				"from":pos,
				"size":size
				})

			elif timefrom == 0 and timeto == 0:
				event = es.search(index="record", body={"query":
				{
					"bool": {
						"must": [
							{"match": {"logLevel": level_q}},
							{"match": {"message": query}}
						]
					}
				},
				"from":pos,
				"size":size
				})
	#search all if logLevel == TRACE			
	elif level_q == "TRACE":
		event = es.search(index="record", body={"query":
				{
					"match_all": {}
				},
				"from":pos,
				"size":size
				})

	return JsonResponse(event)
