from django.contrib import admin

# Register your models here.
from .models import EventLog

class EventLogAdmin(admin.ModelAdmin):
	name = 'EventLog'

admin.site.register(EventLog, EventLogAdmin)